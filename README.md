# Demo_Iaac

Proyecto Demo sobre IaaC preparado por Ricardo Turcios

## Descripción

El proyecto puede ser utilizado como linea base para el despliegue automatizado de un clúster de kubernetes que incluye los componentes y configuraciones base necesarias; el despliegue,se realiza utilizando TERRAFORM (IaaC).

## Contenido de directorios

1. Diagramas: 
Encuentras el diagrama propuesto de la solución.

2. GCP_Demo_Dev: 
Contiene los archivos TERRAFORM base a utilizar para el despligue de un clúster de kubernetes en la nube GCloud.
variables.tf    Puedes ajustar los parámetros de las variables de acuerdo a tus necesidades.
main.tf         Contiene el despliegue de los recursos necesarios en base a las variables.
outputs.tf      La salida a consola que te va a mostrar la IP del clúster luego de ser creado.

3. GCP_Demo_UAT: 
Contiene los archivos TERRAFORM base a utilizar para el despligue de un clúster de kubernetes en la nube GCloud.
variables.tf    Puedes ajustar los parámetros de las variables de acuerdo a tus necesidades.
main.tf         Contiene el despliegue de los recursos necesarios en base a las variables.
outputs.tf      La salida a consola que te va a mostrar la IP del clúster luego de ser creado.

Para comenzar a utilizarlo, es necesario:
1. Debes tener Terraform instalado en tu computadora y funcionando.
2. Clonar el repositorio o copiar los archivos a tu computadora.
3. Tener una cuenta en GCloud.
4. Archivo JSON con Token de acceso IAM al proyecto de nube.
5. Colocar el archivo JSON junto a los archivos y modificar nombre y ruta en el main.tf
6. Ejecutar terraform init
7. Ejecutar terraform plan
8. Ejecutar terraform apply
9. Espera unos minutos, y luego verifica tu clúster.
10. Puedes eliminar el o los despliegues utilizando terraform destroy.

## Autor

Puedes ponerte en contacto con Ricardo Turcios (rturcios77@gmail.com)
