variable "name" {
    default = "gks-demo-uat"
}

variable "project" {
    default = "rturcios77-demo"
}

variable "name-pool" {
    default = "gks-pool1"
}

variable "location" {
    default = "us-central1"
}

variable "initial_node_count" {
    default = 1
}

variable "machine_type" {
    default = "e2-standard-2"
}
